# Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові. Наприклад "The [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'". Слово та номер символу отримайте за допомогою input() або скористайтеся константою. Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in 'Python' is 't' ".

word = input("Введіть слово: ")
length = len(word)
number = input("Введіть номер символа: ")

if number.isdigit():
    symbol_number = int(number)

    if 0 < symbol_number <= length:
        tpl = "{}-й символ у слові '{}' це '{}'"
        my_lst = tpl.format(symbol_number, word, word[symbol_number-1])
        print(my_lst)

    else:
        print("Некоректне введення! Символу з таким номером не існує")

else:
    print("Некоректне введення номеру символа!")


# Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою). Напишіть код, який визначить кількість слів, в цих даних.

text = input("Введіть строку зі слів: ")
words_amount = len(text.split())
tpl = "There are {} words"
res = tpl.format(words_amount)
print(res)


# Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1. Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 3.5, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for element in lst1:
    if type(element) == int:
        lst2.append(element)
    elif type(element) == float:
        lst2.append(element)
print(lst2)




