# 1. Візьміть свою гру з HW8 і перепишіть ії в обʼєктному стилі. Зробіть максимум взаємодії (як явної так і неявної)
# на рівні обʼєктів. Рекомендую подумати над такими класами як Player, GameFigure, Game.
# Памʼятайте про чистоту і простоту коду (DRY, KISS), коментарі та докстрінги.

from abc import ABC, abstractmethod
from random import choice


class Figures:
    rules = {  # key - figure, value - > loser
        'Rock': ('Scissors', 'Lizard'),
        'Scissors': ('Paper', 'Lizard'),
        'Paper': ('Rock', 'Spock'),
        'Lizard': ('Spock', 'Paper'),
        'Spock': ('Scissors', 'Rock')
    }


class Player(ABC):

    @abstractmethod
    def __init__(self):
        pass


class HumanPlayer(Player):
    _human_figure = None

    def __init__(self, human_figure):
        """
        Initializes human player's figure
        """
        self.human_figure = human_figure.lower().capitalize()

# checking if the entering figure is in rules
    @property
    def human_figure(self):
        return self._human_figure

    @human_figure.setter
    def human_figure(self, value):
        if value not in Figures().rules.keys():
            raise Exception

        self._human_figure = value


class AIPlayer(Player):
    ai_figure = None

    def __init__(self):
        """
        Initializes ai player's figure (random choice from rules' figures)
        """
        self.ai_figure = choice(list(Figures().rules.keys()))


class Game:
    human_player_figure = None
    ai_player_figure = None

    def __init__(self, human_player_figure, ai_player_figure):
        self.human_player_figure = human_player_figure
        self.ai_player_figure = ai_player_figure

    def comparation(self):
        """
        Compares the human player's figure and the ai player's one.
        Returns a message about the winner.
        """
        if self.human_player_figure == self.ai_player_figure:
            winner = 'Identical results ---> Draw'
        elif self.ai_player_figure in Figures().rules[self.human_player_figure]:
            winner = 'The winner is ---> Human Player'
        else:
            winner = 'The winner is ---> AI Player'
        return winner

    def game_result(self):
        """
        Finalizes the results of the game.
        Returns the result of the game.
        """
        game_res = f'The Human Player\'s figure is ---> {self.human_player_figure} \
            The AI Player\'s figure is ---> {self.ai_player_figure} \
            {self.comparation()} \n'
        print(game_res)
        return game_res


figure = 'Enter your figure (Rock, Scissors, Paper, Lizard or Spock): '
human_fig = HumanPlayer(input(figure)).human_figure
ai_fig = AIPlayer().ai_figure
game = Game(human_player_figure=human_fig, ai_player_figure=ai_fig)
game.game_result()
