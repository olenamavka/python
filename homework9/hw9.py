# Напишіть декоратор, який вимірює і виводить на екран час виконання функції в секундах і задекоруйте ним
# основну функцію гри з попередньої дз. Після закінчення гри декоратор має сповістити, скільки тривала гра.

from library import get_player_figure
from library import get_ai_figure
from library import define_winner
import time


def decorator_game_time(function):
    """
    The decorator measures and displays the execution time of the game in seconds.
    """

    def wrapper(*args, **kwargs):
        start = time.monotonic()
        result = function(*args, **kwargs)
        game_time = time.monotonic() - start
        print('Game time: %.3f seconds.' % game_time)
        return result

    return wrapper


@decorator_game_time
def game():
    """
    The function collects information about the player's figure. the AI's figure and the winner of the game.
    Returns:
        game_res (str): Returns the results of the game.
    """

    player_figure = get_player_figure()
    ai_figure = get_ai_figure()
    text_message = define_winner(player_figure, ai_figure)
    game_res = f'The player\'s figure is ---> {player_figure} \
    The AI\'s figure is ---> {ai_figure} \
    {text_message} \n'
    print(game_res)
    return game_res


def stat(game_res):
    """
    The function writes statistics to the file.
    Args:
        game_res (str): results of the game
    """

    with open('statistics.txt', 'at') as file:
        file.write(game_res)
    return


game_result = game()
stat(game_result)
