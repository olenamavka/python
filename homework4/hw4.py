# Дана довільна строка. Напишіть код, який знайде в ній і виведе на екран кількість слів, які містять дві голосні літери підряд.

# The vowels in English are a, e, i, o, u, and sometimes y.
# The vowels in Ukrainian are а, е, є, и, і, ї, о, у, ю, я.
# We accept Ukrainian vowels and English vowels with y.


vowels = set('aeiouyаеєиіїоуюя')

text = input("Please, enter your text in English or Ukrainian: ")
user_text = text.lower().split()

word_counter = 0

for word in user_text:
    vowel_counter = 0
    for letter in word:
        if letter in vowels:
            vowel_counter += 1
        else:
            vowel_counter = 0

        if vowel_counter == 2:
            word_counter += 1
            break

tpl = "There are {} words in the text that contain two vowels in a row."
res = tpl.format(word_counter)
print(res)

# Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами:
# { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною.
# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"


lower_limit = 35.9
upper_limit = 37.339
my_dict = {
    "cito": 47.999,
    "BB_studio": 42.999,
    "momo": 49.999,
    "main-service": 37.245,
    "buy.now": 38.324,
    "x-store": 37.166,
    "the_partner": 38.988,
    "store": 37.720,
    "rozetka": 38.003
}
shops_lst = []

for shop, price in my_dict.items():
    if upper_limit > price > lower_limit:
        shops_lst.append(shop)

print(shops_lst)
