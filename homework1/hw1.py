# 1. Задача: Створіть дві змінні first=10, second=30. Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.

first = 10
second = 30

c = first + second
print(c)

d = first - second
print(d)

e = first * second
print(e)

f = first / second
print(f)

g = first % second
print(g)

h = first ** second
print(h)

# 2. Задача: Створіть змінну і почергово запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1. Виведіть на екран результат кожного порівняння.

m = first == second
print(m)

m = first != second
print(m)

m = first > second
print(m)

m = first < second
print(m)

# 3. Задача: Створіть змінну - результат конкатенації (складання) строк str1="Hello " и str2="world"...

str1 = 'Hello '
str2 = 'world'

result = str1 + str2

print(result)
