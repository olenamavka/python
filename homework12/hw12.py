# 1.	Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати тільки обʼєкти класу
# int або float
# 2.	Доопрацюйте класс Line так, щоб в атрибути begin та end обʼєктів цього класу можна було записати тільки обʼєкти
# класу Point
# 3.	Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, аналогічно до класу Line. Визначет метод, що містить площу трикутника.
# Для обчислень можна використати формулу Герона (https://en.wikipedia.org/wiki/Heron%27s_formula)

class Point:
    x = None
    y = None

    def __init__(self, x, y):
        if not isinstance(x, (int, float)):
            raise TypeError
        elif not isinstance(y, (int, float)):
            raise TypeError
        else:
            self.x = x
            self.y = y


class Figure:

    def area(self):
        return self._area()

    def length(self):
        return self._length()

    def _area(self):
        raise NotImplementedError

    def _length(self):
        raise NotImplementedError


class Line(Figure):
    begin = None
    end = None

    def __init__(self, begin, end):
        assert isinstance(begin, Point), 'not Point'
        assert isinstance(end, Point), 'not Point'
        self.begin = begin
        self.end = end

    def length(self):
        res = ((self.begin.x - self.end.x)**2 + (self.begin.y - self.end.y)**2)**0.5
        return res


class Triangle(Figure):
    vertex1 = None
    vertex2 = None
    vertex3 = None

    def __init__(self, vertex_a, vertex_b, vertex_c):
        assert isinstance(vertex_a, Point), 'not Point'
        assert isinstance(vertex_b, Point), 'not Point'
        assert isinstance(vertex_c, Point), 'not Point'
        self.vertex_a = vertex_a
        self.vertex_b = vertex_b
        self.vertex_c = vertex_c

    def area(self):
        a = ((self.vertex_a.x - self.vertex_b.x) ** 2 + (self.vertex_a.y - self.vertex_b.y) ** 2) ** 0.5
        b = ((self.vertex_a.x - self.vertex_c.x) ** 2 + (self.vertex_a.y - self.vertex_c.y) ** 2) ** 0.5
        c = ((self.vertex_b.x - self.vertex_c.x) ** 2 + (self.vertex_b.y - self.vertex_c.y) ** 2) ** 0.5
        p = (a + b + c) * 0.5
        result = (p * (p - a) * (p - b) * (p - c)) ** 0.5
        return result


p1 = Point(0, 0)
p2 = Point(0, 3.0)
p3 = Point(4.0, 0)
my_line = Line(p1, p2)
my_triangle = Triangle(p1, p2, p3)

print(f'Line\'s length: {my_line.length()}')
print(f'Triangle\'s area: {my_triangle.area()}')
