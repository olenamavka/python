# Створіть клас, який реалізує підключення до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ).
# Обʼєкт класу повинен вміти отримувати курс валют станом на певну дату. Обʼєкт класу повинен вміти записати курси
# в текстовий файл. Назва файлу повинна містити дату на яку шукаємо курс, наприклад:
#  21_11_2019.txt
# Дані в файл запишіть у вигляді списку :
# 1. [назва валюти 1] to UAH: [значення курсу до валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу до валюти 2]
# 3. [назва валюти 3] to UAH: [значення курсу до валюти 3]
# ...
# n. [назва валюти n] to UAH: [значення курсу до валюти n]
#
# P.S. Архітектура класу - на розсуд розробника. Не забувайте про DRY, KISS, YAGNI, SRP та перевірки!)

import requests
from datetime import datetime


class CurrencyCourse:
    _date = None

    def __init__(self, date):
        """
        Initializes the date
        """
        self.date = date

    # checking if the date is in allowed period (between 06/01/1996 and today). NBU gives data from 06/01/1996.
    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, value):
        if not datetime(1996, 1, 6).strftime('%Y-%m-%d') <= value.strftime('%Y-%m-%d') <= datetime.now().strftime('%Y-%m-%d'):  # transform dates to similar format
            raise Exception

        self._date = value

    def connect(self):
        """
        Gets response on the request
        """
        date_format1 = self.date.strftime('%Y%m%d')
        course_url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={date_format1}&json'
        try:
            response = requests.get(course_url)
        except Exception as e:
            print(e)
            response = None

        if response:
            if 300 > response.status_code >= 200:
                if 'application/json' in response.headers.get('Content-Type', ''):
                    try:
                        response_json = response.json()
                    except Exception as e:
                        print(e)
                    else:
                        return response_json

    def write_file(self):
        """
        Writes the results to the file
        """
        date_format2 = self.date.strftime('%d-%m-%Y')
        with open(f'{date_format2}.txt', 'wt') as file:
            for i, data in enumerate(self.connect()):
                rate = data.get('rate', {})     # key 'rate' corresponds to the value of the course
                cc = data.get('cc', {})         # key 'cc' corresponds to the currency code
                if not rate or not cc:
                    raise KeyError
                else:
                    res = f'{i + 1}. {cc} to UAH: {rate} \n'
                    file.write(res)



dt = datetime.strptime(input('Enter the date in the format: dd-mm-yyyy, where yyyy is the year, mm is the month, dd is the day\ndate: '), '%d-%m-%Y')
date1 = CurrencyCourse(dt)
course = date1.write_file()
