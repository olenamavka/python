# Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб".
# # Наповніть класи атрибутами на свій розсуд. Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".

class Vehicle:
    manufacture = None
    model = None
    year = None

    def __init__(self, new_manufacture, new_model, new_year):
        self.manufacture = new_manufacture
        self.model = new_model
        self.year = new_year


class Car(Vehicle):
    engine = None
    style = None

    def __init__(self, new_engine, new_style, new_manufacture, new_model, new_year):
        super().__init__(new_manufacture, new_model, new_year)
        self.engine = new_engine
        self.style = new_style


class Airplane(Vehicle):
    airlines = None
    total_flights = None

    def __init__(self, new_airlines, new_total_flights, new_manufacture, new_model, new_year):
        super().__init__(new_manufacture, new_model, new_year)
        self.airlines = new_airlines
        self.total_flights = new_total_flights


class Ship(Vehicle):
    name = None
    swim_area = None

    def __init__(self, new_name, new_swim_area, new_manufacture, new_model, new_year):
        super().__init__(new_manufacture, new_model, new_year)
        self.name = new_name
        self.swim_area = new_swim_area


car1 = Car(new_manufacture='Ford', new_model='Fusion', new_year=2019, new_engine='hybrid', new_style='sedan')
car2 = Car(new_manufacture='BMW', new_model='X5', new_year=2019, new_engine='fuel', new_style='hatchback')
airplane1 = Airplane(new_manufacture='Boeing', new_model='777-300', new_year=2005, new_airlines='Cathay Pacific', new_total_flights=500)
airplane2 = Airplane(new_manufacture='Airbus', new_model='A380', new_year=2010, new_airlines='Emirates', new_total_flights=400)
ship1 = Ship(new_manufacture='Huntington Ingalls Industries', new_model='aircraft carrier', new_year=2022, new_name='Enterprise', new_swim_area='inland')
ship2 = Ship(new_manufacture='Black Sea Shipyard', new_model='frigate', new_year=2022, new_name='Volodymyr Velykyi', new_swim_area='marine')
