# # 1. Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# # Якщо перетворити не вдається функція має повернути 0.

def my_function(arg1):
    while True:
        try:
            value = float(arg1)
        except Exception as e:
            value = 0
        return value


res = my_function(arg1='-5')
print(res)

res = my_function(arg1='abc')
print(res)

# Напишіть функцію, що приймає два аргументи. Функція повинна
# якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів,
# якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
# у будь-якому іншому випадку повернути кортеж з цих аргументів

def my_function(arg1, arg2):
    if isinstance(arg1, (int, float)) and isinstance(arg2, (int, float)):
        value = arg1 * arg2
    elif isinstance(arg1, str) and isinstance(arg2, str):
        value = arg1 + arg2
    else:
        value = (arg1,) + (arg2,)
    return value


res = my_function(
    arg1={"nm": 5},
    arg2=[5, 'kj', 'kjl'])
print(res)

res = my_function(1.2, 5)
print(res)

res = my_function('abc', 'def')
print(res)

# Перепишіть за допомогою функцій вашу программу "Касир в кінотеатрі", яка буде виконувати наступне:
# Попросіть користувача ввести свій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <> <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <> <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <> <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <> <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <> <>, білетів всеодно нема!"
# Замість <> <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік. Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки, 35 - років і тд...). Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "Незважаючи на те, що вам 42 роки, білетів всеодно нема!"


def get_number():
    while True:
        try:
            age = int(input('Enter the age: '))
            if 125 >= age > 0:
                return age
            else:
                print('Invalid value: ')
        except Exception as e:
            print('It\'s not a number')


def ending(age):
    if age % 10 == 1 and age % 100 != 11:
        end = 'рік'
    elif 2 <= age % 10 <= 4 and (age % 100 < 10 or age % 100 > 20):
        end = 'роки'
    else:
        end = 'років'
    return end


def age_validation(age, end):
    if age < 7:
        print(f"Тобі ж {age} {end}! Де твої батьки?")
    elif age % 10 == 7 or age // 10 == 7:
        print(f"Вам {age} {end}, вам пощастить")
    elif age < 16:
        print(f"Тобі лише {age} {end}, а це е фільм для дорослих!")
    elif age > 65:
        print(f"Вам {age} {end}? Покажіть пенсійне посвідчення!")
    else:
        print(f"Незважаючи на те, що Вам {age} {end}, білетів всеодно нема!")


number = get_number()
ending = ending(number)
result = age_validation(number, ending)
