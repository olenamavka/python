from library import exponentiate_multiplication
import pytest


def test_exponentiate_multiplication():
    assert isinstance(exponentiate_multiplication(2, 3, power=2), (int, float)), 'not int or float'


def test_complex():
    assert type(exponentiate_multiplication(2, 3, power=2)) != complex, 'complex'
# complex, e.g. exponentiate_multiplication(-2, 3, power=0.5)


def test_result():
    assert exponentiate_multiplication(-2, 3, power=2.0) == 36, 'not expected result'


def test_type_er():
    with pytest.raises(TypeError):
        exponentiate_multiplication({2}, [2])


def test_zero_division():
    with pytest.raises(ZeroDivisionError):
        exponentiate_multiplication(2.0, 0, power=-2)
