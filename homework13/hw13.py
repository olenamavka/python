# 1	Доопрацюйте всі перевірки на типи даних (x, y в Point, begin, end в Line, etc) - зробіть перевірки
# за допомогою property або класса-дескриптора.
# 2	Доопрацюйте класс Triangle з попередньої домашки наступним чином:
# –	обʼєкти класу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=) за площею.
# –	перетворення обʼєкту классу Triangle на стрінг показує координати його вершин у форматі x1, y1 -- x2, y2 -- x3, y3
# print(str(triangle1))
# 	(1,0 -- 5,9 -- 3,3)


class Point:
    _x = None
    _y = None

    def __init__(self, x, y):
        self.x = x
        self.y = y

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        if not isinstance(value, (int, float)):
            raise Exception

        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        if not isinstance(value, (int, float)):
            raise Exception

        self._y = value


class Figure:

    def area(self):
        return self._area()

    def length(self):
        return self._length()

    def _area(self):
        raise NotImplementedError

    def _length(self):
        raise NotImplementedError


class Line(Figure):
    _begin = None
    _end = None

    def __init__(self, begin, end):
        self.begin = begin
        self.end = end

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, value):
        if not isinstance(value, Point):
            raise Exception

        self._begin = value

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        if not isinstance(value, Point):
            raise Exception

        self._end = value

    def length(self):
        res = ((self.begin.x - self.end.x)**2 + (self.begin.y - self.end.y)**2)**0.5
        return res


class Triangle(Figure):
    _vertex1 = None
    _vertex2 = None
    _vertex3 = None

    def __init__(self, vertex_a, vertex_b, vertex_c):
        self.vertex_a = vertex_a
        self.vertex_b = vertex_b
        self.vertex_c = vertex_c

    @property
    def vertex_a(self):
        return self._vertex_a

    @vertex_a.setter
    def vertex_a(self, value):
        if not isinstance(value, Point):
            raise Exception

        self._vertex_a = value

    @property
    def vertex_b(self):
        return self._vertex_b

    @vertex_b.setter
    def vertex_b(self, value):
        if not isinstance(value, Point):
            raise Exception

        self._vertex_b = value

    @property
    def vertex_c(self):
        return self._vertex_c

    @vertex_c.setter
    def vertex_c(self, value):
        if not isinstance(value, Point):
            raise Exception

        self._vertex_c = value

    def area(self):
        a = ((self.vertex_a.x - self.vertex_b.x) ** 2 + (self.vertex_a.y - self.vertex_b.y) ** 2) ** 0.5
        b = ((self.vertex_a.x - self.vertex_c.x) ** 2 + (self.vertex_a.y - self.vertex_c.y) ** 2) ** 0.5
        c = ((self.vertex_b.x - self.vertex_c.x) ** 2 + (self.vertex_b.y - self.vertex_c.y) ** 2) ** 0.5
        p = (a + b + c) * 0.5
        result = (p * (p - a) * (p - b) * (p - c)) ** 0.5
        return result

    def __eq__(self, other):
        return self.area() == other.area()

    def __ne__(self, other):
        return self.area() != other.area()

    def __gt__(self, other):
        return self.area() > other.area()

    def __ge__(self, other):
        return self.area() >= other.area()

    def __lt__(self, other):
        return self.area() < other.area()

    def __le__(self, other):
        return self.area() <= other.area()

    def __str__(self):
        return f'{self.vertex_a.x},{self.vertex_a.y} -- {self.vertex_b.x},{self.vertex_b.y} -- {self.vertex_c.x},{self.vertex_c.y}'


p1 = Point(0, 0)
p2 = Point(0, 3.0)
p3 = Point(4.0, 0)
p4 = Point(8.0, 0)
my_line = Line(p1, p2)
triangle1 = Triangle(p1, p2, p3)
triangle2 = Triangle(p1, p2, p4)

print(f'Line\'s length: %.3f' % my_line.length())
print(f'Triangle\'s area: %.3f' % triangle1.area())
print(f'Triangle\'s area: %.3f' % triangle2.area())
print(triangle1 <= triangle2)
print(str(triangle1))
