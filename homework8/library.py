# Напишіть гру "rock scissors paper lizard spock". Використайте розділення всієї програми на функції (Single-Responsibility principle).
# Як скелет-заготовку можна використати приклад з заняття.
# До кожної функції напишіть докстрінг або анотацію

from random import choice

rules = {  # key - fig, value - > loser
    'Rock': ('Scissors', 'Lizard'),
    'Scissors': ('Paper', 'Lizard'),
    'Paper': ('Rock', 'Spock'),
    'Lizard': ('Spock', 'Paper'),
    'Spock': ('Scissors', 'Rock')
}


def get_player_figure():
    """
    The function gets player's figure.
        pl_fig (str) is got from Input.
    Returns:
        pl_fig (str): Returns player's figure.
    """

    while True:
        pl_fig = (input('Enter your figure (Rock, Scissors, Paper, Lizard or Spock): ').lower().capitalize())
        if pl_fig in rules.keys():
            return pl_fig
        else:
            print('Invalid value: ')


def get_ai_figure():
    """
    The function gets AI's figure.
        ai_fig (str) is got randomly.
    Returns:
        ai_fig (str): Returns AI's figure.
    """

    ai_fig = choice(list(rules.keys()))
    return ai_fig


def define_winner(pl_fig, ai_fig):
    """
    The function defines the winner of the game.
    Args:
        pl_fig (str): Player's figure
        ai_fig (str): AI's figure

    Returns:
        text_msg (str): Returns a message about the winner of the game.
    """

    if pl_fig == ai_fig:
        text_msg = 'Identical results ---> Draw'
    elif ai_fig in rules[pl_fig]:
        text_msg = 'The winner is ---> Player'
    else:
        text_msg = 'The winner is ---> AI'
    return text_msg
