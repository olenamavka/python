# Візьміть гру з попереднього ДЗ ('rock scissors paper lizard spock') і модифікуйте наступним чином:
# винесіть всі функції в окремий файл (нехай буде library.py) і імпортуцте їх звідти для роботи в основний файл
# додайте запис статистики в файл (які фігури грали і хто переміг на кожному ході), використовуйте open.

from library import get_player_figure
from library import get_ai_figure
from library import define_winner


def game():
    """
    The function collects information about the player's figure. the AI's figure and the winner of the game.
    Returns:
        game_res (str): Returns the results of the game.
    """

    player_figure = get_player_figure()
    ai_figure = get_ai_figure()
    text_message = define_winner(player_figure, ai_figure)
    game_res = f'The player\'s figure is ---> {player_figure} \
    The AI\'s figure is ---> {ai_figure} \
    {text_message} \n'
    return game_res


def stat(game_res):
    """
    The function writes statistics to the file.
    Args:
        game_res (str): results of the game
    """

    with open('statistics.txt', 'at') as file:
        file.write(game_res)
    return


game_result = game()
stat(game_result)
print(game_result)
